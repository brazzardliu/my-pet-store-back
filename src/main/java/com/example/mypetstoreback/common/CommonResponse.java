package com.example.mypetstoreback.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonResponse<T> {
    private final int code;
    private String message;
    private T data;
    private T latexData;

    private CommonResponse(int status, String message){
        this.code = status;
        this.message = message;
    }

    private CommonResponse(int status, T data){
        this.code = status;
        this.data = data;
    }

    public CommonResponse(int status, String message, T data){
        this.code = status;
        this.message = message;
        this.data = data;
    }
    public CommonResponse(int status, String message, T data,T latexData){
        this.code = status;
        this.message = message;
        this.data = data;
        this.latexData = latexData;
    }

    @JsonIgnore
    public boolean isSuccess(){
        return this.code == ResponseCode.SUCCESS.getCode();
    }

    public static <T>CommonResponse<T> createForSuccess(){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc());
    }

    public static <T>CommonResponse<T> createForSuccessMessage(String message){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), message);
    }

    public static <T>CommonResponse<T> createForSuccess(T data){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc(), data);
    }
    public static <T>CommonResponse<T> createForSuccess(T data,T latexData){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc(), data,latexData);
    }
    public static <T>CommonResponse<T> createForSuccess(String message, T data){
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(), message, data);
    }

    public static <T>CommonResponse<T> createForError(){
        return new CommonResponse<>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
    }

    public static <T>CommonResponse<T> createForError(String message){
        return new CommonResponse<>(ResponseCode.ERROR.getCode(), message);
    }

    public static <T>CommonResponse<T> createForError(int code, String message){
        return new CommonResponse<>(code, message);
    }
    public static <T>CommonResponse<T> createForErrorMessage(String message){
        return new CommonResponse<>(ResponseCode.ERROR.getCode() , message);
    }
    public static <T>CommonResponse<T> createForErrorData(T data){
        return new CommonResponse<>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc(), data);
    }
}
