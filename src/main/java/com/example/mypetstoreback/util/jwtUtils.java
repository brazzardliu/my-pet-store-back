package com.example.mypetstoreback.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;

public class jwtUtils {
    private static final String SIGN = "!Q@W#E$R";
    //生成Token
    public static String getToken(String username){
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE,7);
        String token = JWT.create()
                .withClaim("username",username)
                .withExpiresAt(instance.getTime())
                .sign(Algorithm.HMAC256(SIGN));
        return token;
    }

    //验证token
    public static DecodedJWT verity(String token){
        return JWT.require(Algorithm.HMAC256(SIGN)).build().verify(token);
    }
}
