package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.orderStatus;
import org.springframework.stereotype.Repository;

@Repository
public interface orderStatusMapper extends BaseMapper<orderStatus> {
}
