package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.Product;

public interface productMapper extends BaseMapper<Product> {
}
