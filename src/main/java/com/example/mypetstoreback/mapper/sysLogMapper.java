package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.logInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface sysLogMapper extends BaseMapper<logInfo> {
}
