package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.orderCartItem;
import org.springframework.stereotype.Repository;

@Repository
public interface orderCartItemMapper extends BaseMapper<orderCartItem> {
}
