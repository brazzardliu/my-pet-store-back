package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface categoryMapper extends BaseMapper<Category> {
}
