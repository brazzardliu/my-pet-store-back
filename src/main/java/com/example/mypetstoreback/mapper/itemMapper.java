package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.Item;
import org.springframework.stereotype.Repository;

@Repository
public interface itemMapper extends BaseMapper<Item> {
}
