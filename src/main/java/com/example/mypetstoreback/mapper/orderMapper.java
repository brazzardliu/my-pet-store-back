package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface orderMapper extends BaseMapper<Order> {
}
