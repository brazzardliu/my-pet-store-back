package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface accountMapper extends BaseMapper<Account> {
}
