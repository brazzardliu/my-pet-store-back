package com.example.mypetstoreback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mypetstoreback.pojo.AccountInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface accountInfoMapper extends BaseMapper<AccountInfo> {
}
