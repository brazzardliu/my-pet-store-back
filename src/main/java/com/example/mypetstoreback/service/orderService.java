package com.example.mypetstoreback.service;

import com.example.mypetstoreback.pojo.Order;

public interface orderService {
    //通过orderId获取订单
    public Order selectOrderByOrderId(int orderId);
    //通过orderId删除订单
    public int deleteByOrderId(int orderId);
    //通过新加的Order更新旧的Order
    public int updateByOrderId(int orderId,Order order);
}
