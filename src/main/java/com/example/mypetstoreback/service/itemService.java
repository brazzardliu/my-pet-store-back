package com.example.mypetstoreback.service;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.pojo.Item;
import com.example.mypetstoreback.pojo.Product;

import java.util.List;

public interface itemService {
    //通过productId查看item
    public CommonResponse<List<Item>> getItemByProductId(String productid);
    //新增item
    public CommonResponse<Item> addItem(Item item);
    //修改item
    public CommonResponse<String> updateItem(Item item);
    //删除item
    public CommonResponse<String> deleteItemByItemId(String itemId);
    //根据shipname获取itemid
    public List<String> getItemIdListByShipName(String shipName);
}
