package com.example.mypetstoreback.service;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.pojo.Product;

import java.util.List;

public interface productService {
    //通过categoryId查询商品
    public CommonResponse<List<Product>> getProductByCategoryId(String categoryid);
    //新增商品
    public CommonResponse<Product> addProduct(Product product);
    //修改商品
    public CommonResponse<String> updateProductByProductId(Product product);
    //删除商品
    public CommonResponse<String> deleteProductByProductId(String productId);
}
