package com.example.mypetstoreback.service;

import com.example.mypetstoreback.pojo.AccountInfo;

public interface accountInfoService {
    //通过用户名获取用户详细信息
    public AccountInfo getAccountInfoByUserName(String username);
    //注册信息即插入数据库
    public String setAccountInfo(AccountInfo accountInfo);
    //更新数据库
    public void updateAccountInfoByUserName(AccountInfo accountInfo,String username);
}
