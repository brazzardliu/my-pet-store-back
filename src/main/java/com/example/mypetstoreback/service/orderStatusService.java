package com.example.mypetstoreback.service;

import com.example.mypetstoreback.pojo.orderStatus;

public interface orderStatusService {
    //通过orderId来获取订单状态
    public String gerOrderStatusByOrderId(int orderId);
    //修改订单状态
    public int updateOrderStatusByOrderStatus(orderStatus orderStatus,int orderId);
}
