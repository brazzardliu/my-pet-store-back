package com.example.mypetstoreback.service;

import com.example.mypetstoreback.pojo.orderCartItem;

import java.util.List;

public interface orderCartItemService {
    //修改订单内容
    public void updateOrderCartItem(List<orderCartItem> orderCartItems, int orderId);
    //通过itemIds获取订单信息
    public List<orderCartItem> getOrderCartItemByItemIds(List<String> itemIds);
    //通过orderId来获取订单信息
    public List<orderCartItem> getOrderCartItemByOrderId(int orderId);
}
