package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.mapper.orderStatusMapper;
import com.example.mypetstoreback.pojo.orderStatus;
import com.example.mypetstoreback.service.orderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class orderStatusServiceImpl implements orderStatusService {
    @Autowired
    private orderStatusMapper orderStatusMapper;
    @Override
    public String gerOrderStatusByOrderId(int orderId) {
        orderStatus orderStatus = orderStatusMapper.selectById(orderId);
        if (orderStatus != null)
            return orderStatus.getStatus();
        else
            return null;
    }

    @Override
    public int updateOrderStatusByOrderStatus(orderStatus orderStatus,int orderId) {
        QueryWrapper<orderStatus> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderid",orderId);
        int count = orderStatusMapper.update(orderStatus,queryWrapper);
        return count;
    }
}
