package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.mapper.itemMapper;
import com.example.mypetstoreback.pojo.Item;
import com.example.mypetstoreback.service.itemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import java.util.List;

@Service
public class itemServiceImpl implements itemService {
    @Autowired
    private itemMapper itemMapper;
    @Override
    public List<String> getItemIdListByShipName(String shipName) {
        QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("shipname", shipName);
        List<Item> items = itemMapper.selectList(queryWrapper);
        List<String> itemIds = new ArrayList<>();
        for (Item item : items) {
            itemIds.add(item.getItemId());
        }
        return itemIds;
    }
    @Override
    public CommonResponse<List<Item>> getItemByProductId(String productid) {
        QueryWrapper<Item> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("productid",productid);
        List<Item> items = itemMapper.selectList(queryWrapper);
        return CommonResponse.createForSuccess(items);
    }

    @Override
    public CommonResponse<Item> addItem(Item item) {
        int result = itemMapper.insert(item);
        if (result==1){
            return CommonResponse.createForSuccess(item);
        }else {
            return CommonResponse.createForError("服务器异常");
        }
    }

    @Override
    public CommonResponse<String> updateItem(Item item) {
        int result = itemMapper.updateById(item);
        if (result==1){
            return CommonResponse.createForSuccess("修改成功！");
        }else {
            return CommonResponse.createForError("修改失败！");
        }
    }

    @Override
    public CommonResponse<String> deleteItemByItemId(String itemId) {
        int result = itemMapper.deleteById(itemId);
        if (result==1){
            return CommonResponse.createForSuccess("删除成功！");
        }else {
            return CommonResponse.createForError("删除失败！");
        }
    }
}
