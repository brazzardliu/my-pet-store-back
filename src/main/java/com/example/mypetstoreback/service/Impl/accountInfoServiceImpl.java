package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.mapper.accountInfoMapper;
import com.example.mypetstoreback.mapper.accountMapper;
import com.example.mypetstoreback.pojo.Account;
import com.example.mypetstoreback.pojo.AccountInfo;
import com.example.mypetstoreback.service.accountInfoService;
import com.example.mypetstoreback.service.accountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class accountInfoServiceImpl implements accountInfoService {
    @Autowired
    private accountInfoMapper accountinfomapper;
    @Autowired
    private accountService accountService;
    @Autowired
    private accountMapper accountMapper;
    @Override
    public AccountInfo getAccountInfoByUserName(String username) {
//        QueryWrapper<AccountInfo> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("username",username);
//        AccountInfo accountInfo = accountinfomapper.selectOne(queryWrapper);
        return accountinfomapper.selectById(username);
    }

    @Override
    public String setAccountInfo(AccountInfo accountInfo) {
        if (accountService.isExistUserName(accountInfo.getUsername()) == "exist")
            return "用户名已存在！";
        accountinfomapper.insert(accountInfo);
        Account account = new Account();
        account.setUsername(accountInfo.getUsername());
        account.setPassword(accountInfo.getPassword());
        accountService.setAccount(account);
        return null;
    }

    @Override
    public void updateAccountInfoByUserName(AccountInfo accountInfo, String username) {
        Account account  = new Account();
        account.setUsername(accountInfo.getUsername());
        account.setPassword(accountInfo.getPassword());
        accountinfomapper.deleteById(username);
        accountinfomapper.insert(accountInfo);
        accountMapper.deleteById(username);
        accountMapper.insert(account);
    }
}
