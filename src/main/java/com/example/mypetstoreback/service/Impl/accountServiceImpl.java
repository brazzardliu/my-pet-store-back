package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.mapper.accountMapper;
import com.example.mypetstoreback.pojo.Account;
import com.example.mypetstoreback.service.accountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class accountServiceImpl implements accountService {
    @Autowired
    private accountMapper mapper;
    @Override
    public String isRightOfAccount(String username, String password) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper();
        queryWrapper.eq("username",username).eq("password",password);
        Account account = mapper.selectOne(queryWrapper);
        if (account != null)
            return "success";
        else
            return "用户名或密码错误！";
    }

    @Override
    public String isExistUserName(String username) {
        Account account = mapper.selectById(username);
        if (account != null)
            return "exist";
        else
            return "no exist";
    }

    @Override
    public void setAccount(Account account) {
        mapper.insert(account);
    }
}
