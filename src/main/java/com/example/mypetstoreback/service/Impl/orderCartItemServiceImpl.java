package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.mapper.orderCartItemMapper;
import com.example.mypetstoreback.pojo.orderCartItem;
import com.example.mypetstoreback.service.orderCartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class orderCartItemServiceImpl implements orderCartItemService {
    @Autowired
    private orderCartItemMapper orderCartItemMapper;
    //通过orderCartItem来更新数据
    @Override
    public void updateOrderCartItem(List<orderCartItem> orderCartItems, int orderId) {
        for (orderCartItem orderCartItem :orderCartItems){
            orderCartItem.setOrderid(orderId);
            QueryWrapper<orderCartItem> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("orderid",orderId).eq("itemid",orderCartItem.getItemid());
            orderCartItemMapper.update(orderCartItem,queryWrapper);
        }
    }

    @Override
    public List<orderCartItem> getOrderCartItemByItemIds(List<String> itemIds) {
        List<orderCartItem> orderCartItems = new ArrayList<>();
        //通过map进行数据库的查询
        Map<String,Object> map = new HashMap<>();
        for (String itemId : itemIds){
            map.put("itemid",itemId);
            List<orderCartItem> orderCartItemPart = orderCartItemMapper.selectByMap(map);
            System.out.println("orderCartItemPart...."+orderCartItemPart);
            orderCartItems.addAll(orderCartItemPart);
            map.clear();
        }
        return orderCartItems;
    }

    @Override
    public List<orderCartItem> getOrderCartItemByOrderId(int orderId) {
        QueryWrapper<orderCartItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderid",orderId);
        return orderCartItemMapper.selectList(queryWrapper);
    }
}
