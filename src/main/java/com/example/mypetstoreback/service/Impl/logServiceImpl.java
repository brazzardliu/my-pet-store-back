package com.example.mypetstoreback.service.Impl;


import com.example.mypetstoreback.mapper.sysLogMapper;
import com.example.mypetstoreback.pojo.logInfo;
import com.example.mypetstoreback.service.logService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class logServiceImpl implements logService {
    @Autowired
    private sysLogMapper sysLogMapper;
    @Override
    public void saveLog(logInfo logInfo) {
        Date date = new Date(System.currentTimeMillis());
        logInfo.setInsertTime(date);
        logInfo.setUpdateTime(date);
        sysLogMapper.insert(logInfo);
    }
}
