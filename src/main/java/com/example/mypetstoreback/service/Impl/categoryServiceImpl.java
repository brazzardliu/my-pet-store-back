package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.mapper.categoryMapper;
import com.example.mypetstoreback.mapper.itemMapper;
import com.example.mypetstoreback.mapper.productMapper;
import com.example.mypetstoreback.pojo.Category;
import com.example.mypetstoreback.pojo.Item;
import com.example.mypetstoreback.pojo.Product;
import com.example.mypetstoreback.service.categoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class categoryServiceImpl implements categoryService {
    @Autowired
    private categoryMapper categoryMapper;
    @Autowired
    private productMapper productMapper;
    @Autowired
    private itemMapper itemMapper;

    @Override
    public CommonResponse<List<Category>> getCategories() {
        List<Category> categories = categoryMapper.selectList(null);
        return CommonResponse.createForSuccess(categories);
    }

    @Override
    public CommonResponse<Category> addCategory(Category category) {
        int result = categoryMapper.insert(category);
        if (result == 1) {
            return CommonResponse.createForSuccess(category);
        } else {
            return CommonResponse.createForError("服务器异常");
        }
    }

    @Override
    public CommonResponse<String> updateCategory(Category category) {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("catid",category.getCategoryId());
        int result = categoryMapper.update(category,queryWrapper);
        if (result == 1)
            return CommonResponse.createForSuccess();
        else
            return CommonResponse.createForError();
    }


    @Override
    public CommonResponse<String> deleteCategoryByCategoryId(String categoryId) {
        //实现数据库表之间的约束，将category删除对应的product和item表也要删除；
        try {
            // 查询与类别相关的产品
            QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
            productQueryWrapper.eq("category", categoryId);
            List<Product> products = productMapper.selectList(productQueryWrapper);

            // 删除与类别相关的产品和条目
            for (Product product : products) {
                // 删除产品对应的条目
                QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
                itemQueryWrapper.eq("productId", product.getProductId());
                itemMapper.delete(itemQueryWrapper);
                // 删除产品
                productMapper.deleteById(product.getProductId());
            }

            // 删除类别
            int result = categoryMapper.deleteById(categoryId);

            if (result > 0) {
                return CommonResponse.createForSuccess("删除成功！");
            } else {
                return CommonResponse.createForError("删除失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResponse.createForError("删除失败！");
        }
    }
}