package com.example.mypetstoreback.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mypetstoreback.mapper.orderCartItemMapper;
import com.example.mypetstoreback.mapper.orderMapper;
import com.example.mypetstoreback.pojo.Order;
import com.example.mypetstoreback.pojo.orderCartItem;
import com.example.mypetstoreback.service.orderCartItemService;
import com.example.mypetstoreback.service.orderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class orderServiceImpl implements orderService {
    @Autowired
    private orderMapper mapper;
    @Autowired
    private orderCartItemMapper orderCartItemMapper;
    @Autowired
    private orderCartItemService orderCartItemService;
    //通过ID找订单
    @Override
    public Order selectOrderByOrderId(int orderId) {
        Order order = mapper.selectById(orderId);
        order.setOrdercartitem(orderCartItemService.getOrderCartItemByOrderId(orderId));
        return order;
    }
    //通过orderId删除订单
    @Override
    public int deleteByOrderId(int orderId) {
        int count = mapper.deleteById(orderId);
        QueryWrapper<orderCartItem> queryWrapper = new QueryWrapper<>();
        //删除在linenum表内的数据
        queryWrapper.eq("orderid",orderId);
        orderCartItemMapper.delete(queryWrapper);
        return count;
    }
    //通过orderID来更新表内数据
    @Override
    public int updateByOrderId(int orderId, Order order) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderid",orderId);
        order.setOrderid(orderId);
        int count = mapper.update(order,queryWrapper);
        return count;
    }
}
