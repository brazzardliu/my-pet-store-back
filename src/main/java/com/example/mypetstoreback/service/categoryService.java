package com.example.mypetstoreback.service;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.pojo.Category;
import com.example.mypetstoreback.pojo.Product;

import java.util.List;

public interface categoryService {
    //查看category
    public CommonResponse<List<Category>> getCategories();
    //新增category
    public CommonResponse<Category> addCategory(Category category);
    //修改category
    public CommonResponse<String> updateCategory(Category category);
    //删除category
    public CommonResponse<String> deleteCategoryByCategoryId(String categoryId);
}
