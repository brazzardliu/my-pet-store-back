package com.example.mypetstoreback.service;

import com.example.mypetstoreback.pojo.Account;

public interface accountService {
    //判断账户是否正确
    public String isRightOfAccount(String username,String password);
    //判断用户名是否已存在
    public String isExistUserName(String username);
    //插入用户信息
    public void setAccount(Account account);
}
