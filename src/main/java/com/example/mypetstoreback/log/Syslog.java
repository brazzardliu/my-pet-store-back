package com.example.mypetstoreback.log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


//定义一个注解使所有需要记录日志的操作都通过注解来进行获取并放入数据库
@Target({ElementType.METHOD,ElementType.PARAMETER}) // 注解能放置的位置
@Retention(RetentionPolicy.RUNTIME) // 注解实例是在该注解被访问时创建且保存在运行内容中（临时对象）
public @interface Syslog {
    /**
     * 模块名称
     * @return
     */
    String modelName() default "";

    /**
     * 方法类型
     * @return
     */
    int methodType() default 1;

    /**
     * 方法描述
     * @return
     */
    String methodDesc() default "";

    /**
     * 系统类型：1：web调用 2：APP调用
     * @return
     */
    int sysType() default 1;
}
