package com.example.mypetstoreback.controller;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.log.Syslog;
import com.example.mypetstoreback.log.SyslogConfig;
import com.example.mypetstoreback.pojo.Order;
import com.example.mypetstoreback.pojo.orderCartItem;
import com.example.mypetstoreback.pojo.orderStatus;
import com.example.mypetstoreback.service.itemService;
import com.example.mypetstoreback.service.orderCartItemService;
import com.example.mypetstoreback.service.orderService;
import com.example.mypetstoreback.service.orderStatusService;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class orderController {
    @Autowired
    private orderService orderservice;
    @Autowired
    private orderCartItemService orderCartItemService;
    @Autowired
    private itemService itemService;
    @Autowired
    private orderStatusService orderStatusService;
    //通过OrderId获取订单
    @GetMapping("/orders/{orderid}")
    @Syslog(modelName = "获取订单",methodDesc = "获取订单详细信息")
    public CommonResponse<Order> getOrder(@PathVariable("orderid") int orderId){
        System.out.println("获取订单"+orderId);
        Order order  = orderservice.selectOrderByOrderId(orderId);
        System.out.println(order);
        if (order == null)
            return CommonResponse.createForSuccessMessage("搜索失败！");
        else
            return CommonResponse.createForSuccess(order);
    }
    //通过OrderId删除订单
    @DeleteMapping("/orders/{orderid}")
    @Syslog(modelName = "删除订单",methodDesc = "删除订单")
    public CommonResponse<String> deleteOrder(@PathVariable("orderid") int orderId){
        int state = orderservice.deleteByOrderId(orderId);
        System.out.println("state:"+state);
        if (state == 1)
            return CommonResponse.createForSuccess("删除成功！");
        else
            return CommonResponse.createForSuccess("删除失败！");
    }
    //通过Order修改订单
    @PutMapping("/orders/{orderid}")
    @Syslog(modelName = "修改订单",methodDesc = "修改订单")
    public CommonResponse<String> updateOrder(@PathVariable("orderid") int orderId ,@RequestBody Order order){
        List<orderCartItem> ordercartitems = order.getOrdercartitem();
        orderservice.updateByOrderId(orderId,order);
        orderCartItemService.updateOrderCartItem(ordercartitems,orderId);
        return CommonResponse.createForSuccess();
    }
    //查看用户的订单列表
    @GetMapping("/orders")
    @Syslog(modelName = "查看订单",methodDesc = "查看订单列表")
    public CommonResponse<List<orderCartItem>> getOrderList(HttpServletRequest request){
        //获取用户名
        String username = SyslogConfig.getUserNameByToken(request);

        //获取商家所上架的所有商品
        List<String> itemIds = itemService.getItemIdListByShipName(username);
        //通过itemIds获取所有商家的订单信息
        List<orderCartItem> orderCartItems = orderCartItemService.getOrderCartItemByItemIds(itemIds);

        return CommonResponse.createForSuccess(orderCartItems);
    }
    //查看订单状态
    @GetMapping("statusOfOrder/{orderid}")
    @Syslog(modelName = "查看订单状态",methodDesc = "获取订单详细状态")
    public CommonResponse<String> getOrderStatus(@PathVariable("orderid") int orderId){
        String status = orderStatusService.gerOrderStatusByOrderId(orderId);
        if (status != null)
            return CommonResponse.createForSuccess(status);
        else
            return CommonResponse.createForError("orderId is wrong!");
    }
    //修改订单状态
    @PutMapping("statusOfOrder/{orderid}")
    @Syslog(modelName = "修改订单状态",methodDesc = "修改订单状态")
    public CommonResponse<String> updateOrderStatusByOrderStatus(@PathVariable("orderid") int orderId, @RequestBody orderStatus orderStatus){
        int count = orderStatusService.updateOrderStatusByOrderStatus(orderStatus,orderId);
        System.out.println("count...."+count);
        if (count > 0)
            return CommonResponse.createForSuccess();
        else
            return CommonResponse.createForError();
    }
}
