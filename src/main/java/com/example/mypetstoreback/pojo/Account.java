package com.example.mypetstoreback.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("businessaccount")
public class Account {
    @TableId
    private String username;
    private String password;
}
