package com.example.mypetstoreback.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.github.jeffreyning.mybatisplus.anno.MppMultiId;
import lombok.Data;

import java.math.BigDecimal;

@TableName("lineitem")
@Data
public class orderCartItem {
//    @MppMultiId // 复合主键
    @TableField("orderid")
    private int orderid;
//    @MppMultiId // 复合主键
    @TableField("itemid")
    private String itemid;
    private int linenum;
    private int quantity;
    private BigDecimal unitprice;
    private BigDecimal price;
}
