package com.example.mypetstoreback.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("businessinfo")
public class AccountInfo {
    @TableId
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String address;
    private String city;
    private String phone;
}
