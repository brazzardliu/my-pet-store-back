package com.example.mypetstoreback.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("t_log")
@Data
public class logInfo {
    @TableId("logID")
    private int logID;
    @TableField("insertIP")
    private String insertIP;
    @TableField("isDelete")
    private int isDelete;
    @TableField("insertTime")
    private Date insertTime;
    @TableField("updateTime")
    private Date updateTime;
    @TableField("comment")
    private String comment;
    @TableField("accountName")
    private String accountName;
    @TableField("path")
    private String path;
    @TableField("getTime")
    private Date getTime;
    @TableField("overTime")
    private Date overTime;
    @TableField("doTime")
    private  String doTime;
    @TableField("methodName")
    private String methodName;
    @TableField("methodType")
    private int methodType;
    @TableField("methodParam")
    private String methodParam;
    @TableField("methodDesc")
    private String methodDesc;
    @TableField("modelName")
    private String modelName;
    @TableField("sysType")
    private int sysType;
    @TableField("resType")
    private int resType;
    @TableField("ycMsg")
    private String ycMsg;
}
