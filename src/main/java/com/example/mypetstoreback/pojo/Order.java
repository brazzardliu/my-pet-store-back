package com.example.mypetstoreback.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@TableName("orders")
@Data
public class Order {
    @TableId(value = "orderid")
    private int orderid;
    @TableField(value = "userid")
    private String userid;
    @TableField("orderdate")
    private Date orderdate;
    @TableField("shipaddr1")
    private String shipaddr1;
    @TableField("shipaddr2")
    private String shipaddr2;
    @TableField("shipcity")
    private String shipcity;
    @TableField("shipstate")
    private String shipstate;
    @TableField("shipzip")
    private String shipzip;
    @TableField("shipcountry")
    private String shipcountry;
    @TableField("billaddr1")
    private String billaddr1;
    @TableField("billaddr2")
    private String billaddr2;
    @TableField("billcity")
    private String billcity;
    @TableField("billstate")
    private String billstate;
    @TableField("billzip")
    private String billzip;
    @TableField("billcountry")
    private String billcountry;
    private String courier;
    @TableField("totalprice")
    private BigDecimal totalprice;
    @TableField("billtofirstname")
    private String billtofirstname;
    @TableField("billtolastname")
    private String billtolastname;
    @TableField("shiptofirstname")
    private String shiptofirstname;
    @TableField("shiptolastname")
    private String shiptolastname;
    @TableField("creditcard")
    private String creditcard;
    @TableField("exprdate")
    private String expirydate;
    @TableField("cardtype")
    private String cardtype;
    private String locale;
    @TableField(exist = false)
    private String status;
    @TableField(exist = false)
    private List<orderCartItem> ordercartitem;
}
