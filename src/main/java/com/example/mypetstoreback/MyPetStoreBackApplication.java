package com.example.mypetstoreback;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@SpringBootApplication
@MapperScan("com.example.mypetstoreback.mapper")
public class MyPetStoreBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyPetStoreBackApplication.class, args);
    }
}
