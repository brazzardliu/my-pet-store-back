package com.example.mypetstoreback.controller;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.log.Syslog;
import com.example.mypetstoreback.mapper.categoryMapper;
import com.example.mypetstoreback.pojo.Category;
import com.example.mypetstoreback.pojo.Item;
import com.example.mypetstoreback.service.categoryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
public class CategoryController {
    @Autowired
    private categoryService categoryService;
    @GetMapping("/categories")
    @Syslog(modelName = "查询商品",methodDesc = "后台查询所有商品(category)")
    public CommonResponse<List<Category>> getCategories(){
        return categoryService.getCategories();
    }

    @PostMapping("/category")
    @Syslog(modelName = "添加商品",methodDesc = "后台添加商品(category)")
    public CommonResponse<Category> addCategory(@Valid @RequestBody Category category) {
        return categoryService.addCategory(category);
    }
    @DeleteMapping("/categories/{catid}")
    @Syslog(modelName = "删除商品",methodDesc = "后台删除商品(category)")
    public CommonResponse<String> deleteProductByProductId(@PathVariable("catid") String categoryId){
        return categoryService.deleteCategoryByCategoryId(categoryId);
    }
    @PutMapping("/categories")
    @Syslog(modelName = "修改商品",methodDesc = "后台修改商品(item)")
    public CommonResponse<String> updateCategory(@Valid @RequestBody Category category){
        return categoryService.updateCategory(category);
    }
}
