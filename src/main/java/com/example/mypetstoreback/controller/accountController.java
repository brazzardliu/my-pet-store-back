package com.example.mypetstoreback.controller;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.log.Syslog;
import com.example.mypetstoreback.log.SyslogConfig;
import com.example.mypetstoreback.pojo.AccountInfo;
import com.example.mypetstoreback.service.accountInfoService;
import com.example.mypetstoreback.service.accountService;
import com.example.mypetstoreback.util.jwtUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;

@RestController
@Validated
public class accountController {
    @Autowired
    private accountService accountService;
    @Autowired
    private accountInfoService accountInfoService;
    //登录
    @PostMapping("/login")
    @Syslog(modelName = "请求登录",methodDesc = "后台请求登录")
    public CommonResponse<String> CheckUser(@RequestParam("username") @NotBlank(message = "用户名不能为空") String username,
                                            @RequestParam("password") @NotBlank(message = "密码不能为空") String password, HttpServletRequest request){
        String msg = accountService.isRightOfAccount(username,password);
        System.out.println("username"+username);
        if (msg.equals("success"))
        {
            String token = jwtUtils.getToken(username);
            HttpSession session = request.getSession();
            session.setAttribute("token",token);
            return CommonResponse.createForSuccess(token);
        }
        else
            return CommonResponse.createForErrorData(msg);
    }
    //获取账户信息
    @Syslog(modelName = "获取信息",methodDesc = "后台请求获取用户信息")
    @GetMapping("/users/{username}")
    public CommonResponse<AccountInfo> getAccountInfo(@PathVariable("username") String username){
        AccountInfo accountInfo = accountInfoService.getAccountInfoByUserName(username);
        System.out.println("GetAccountInfo"+accountInfo);
        if (accountInfo != null)
            return CommonResponse.createForSuccess(accountInfo);
        else
            return CommonResponse.createForErrorMessage("用户名错误！");
    }
    //请求注册
    @PostMapping("/users")
    @Syslog(modelName = "注册",methodDesc = "后台请求注册")
    public CommonResponse<String> checkRegister(AccountInfo accountInfo){
        String msg = accountInfoService.setAccountInfo(accountInfo);
        if (msg == null)
            return CommonResponse.createForSuccess();
        else
            return CommonResponse.createForErrorData(msg);
    }
    //检查用户名
    @GetMapping("/ajax/users")
    @Syslog(modelName = "检查用户名",methodDesc = "后台请求检查用户名")
    public CommonResponse<String> checkUserName(@RequestParam("username") String username){
        System.out.println(11111);
        String msg = accountService.isExistUserName(username);
        return CommonResponse.createForSuccess(msg);
    }
    //更新用户信息
    @PutMapping("/messageUpdate")
    @Syslog(modelName = "请求更新",methodDesc = "后台请求更新用户信息")
    public CommonResponse<String> updateAccountInfo(@RequestBody AccountInfo accountInfo,HttpServletRequest request){
        System.out.println("进入请求更新用户信息");
        System.out.println("AccountInfo"+accountInfo);
        String username = SyslogConfig.getUserNameByToken(request);
        accountInfoService.updateAccountInfoByUserName(accountInfo,username);
        //生成新的token
        String token = jwtUtils.getToken(accountInfo.getUsername());
        return CommonResponse.createForSuccess(token);
    }
}
