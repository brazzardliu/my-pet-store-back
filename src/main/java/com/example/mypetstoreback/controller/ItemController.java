package com.example.mypetstoreback.controller;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.log.Syslog;
import com.example.mypetstoreback.pojo.Item;
import com.example.mypetstoreback.pojo.Product;
import com.example.mypetstoreback.service.categoryService;
import com.example.mypetstoreback.service.itemService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
public class ItemController {
    @Autowired
    private itemService itemService;

    //查询商品
    @GetMapping("/items/{productid}")
    @Syslog(modelName = "查询商品", methodDesc = "后台通过productid查询商品(item)")
    public CommonResponse<List<Item>> getItems(@PathVariable("productid") String productid) {
        System.out.println("productId:"+productid);
        return itemService.getItemByProductId(productid);
    }

    @PostMapping("/items")
    @Syslog(modelName = "添加商品", methodDesc = "后台添加商品(item)")
    public CommonResponse<Item> addItem(@Valid @RequestBody Item item) {
        return itemService.addItem(item);
    }

    @DeleteMapping("/items/{itemid}")
    @Syslog(modelName = "删除商品", methodDesc = "后台删除商品(item)")
    public CommonResponse<String> deleteProductByProductId(@PathVariable("itemid") String itemId) {
        return itemService.deleteItemByItemId(itemId);
    }
    @PutMapping("/items")
    @Syslog(modelName = "修改商品",methodDesc = "后台修改商品(item)")
    public CommonResponse<String> updateItem(@Valid @RequestBody Item item){
        System.out.println(item);
        return itemService.updateItem(item);
    }
}
