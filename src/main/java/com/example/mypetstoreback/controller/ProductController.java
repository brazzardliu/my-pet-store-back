package com.example.mypetstoreback.controller;

import com.example.mypetstoreback.common.CommonResponse;
import com.example.mypetstoreback.log.Syslog;
import com.example.mypetstoreback.pojo.Account;
import com.example.mypetstoreback.pojo.Product;
import com.example.mypetstoreback.service.productService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
public class ProductController {
    @Autowired
    private productService productService;
    @GetMapping("/products/{categoryid}")
    @Syslog(modelName = "查询商品",methodDesc = "后台通过categoryId查询商品(product)")
    public CommonResponse<List<Product>> getProducts(@PathVariable("categoryid") String categoryid){
        return productService.getProductByCategoryId(categoryid);
    }
    @PostMapping("/products")
    @Syslog(modelName = "添加商品",methodDesc = "后台添加商品(product)")
    public CommonResponse<Product> addProduct( @RequestBody Product product){
        return productService.addProduct(product);
    }
    @DeleteMapping("/products/{productid}")
    @Syslog(modelName = "删除商品",methodDesc = "后台删除商品(product)")
    public CommonResponse<String> deleteProductByProductId(@PathVariable("productid") String productId){
        return productService.deleteProductByProductId(productId);
    }
    @PutMapping("/products/{productid}")
    @Syslog(modelName = "修改商品",methodDesc = "后台修改商品(product)")
    public CommonResponse<String> updateProduct(@RequestBody Product product){
        return productService.updateProductByProductId(product);
    }
}